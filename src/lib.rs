#[cfg(test)]
mod tests {

    use dracaena::ast::common::uppercase_first_letter;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn it_works_with_deps() {
        assert_eq!("Hello", uppercase_first_letter("hello"));
    }

    #[test]
    fn it_works_and_will_be_cached() {
        assert_eq!("Hello", uppercase_first_letter("hello"));
    }
}
